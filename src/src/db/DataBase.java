package src.db;

import src.model.Graphic;

import javax.ejb.Stateless;
import java.util.LinkedList;
import java.util.List;

@Stateless
public class DataBase {
    private List<Graphic> graphicList = new LinkedList<>();

    public List<Graphic> getGraphicList() {
        return graphicList;
    }
}
