package src.service;

import src.db.DataBase;
import src.model.Graphic;
import src.service.mapper.ImageMapper;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class GraphicGalleryService {

    private List<String> imageList = new ArrayList<>();
    private String image = null;

    @EJB
    private ImageMapper imageMapper;

    @EJB
    private DataBase dataBase;

    public List<String> getImageList() {
        imageList.clear();
        for (Graphic element : dataBase.getGraphicList()) {
            image = imageMapper.fileToImage(element.getFile());
            if (image != null && !image.isEmpty()) {
                imageList.add(image);
            }
        }
        return imageList;
    }
}
