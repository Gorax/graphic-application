package src.service;

import src.db.DataBase;
import src.model.Graphic;
import src.service.mapper.ImageMapper;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

@Stateless
public class SingleGraphicFileService {

    @EJB
    private DataBase dataBase;

    @EJB
    private ImageMapper imageMapper;

    private OutputStream outputStream = null;
    private String image = null;
    private int index;

    public String singleImage(int index) {
        this.index = index;
        Graphic graphic = dataBase.getGraphicList().get(index);
        image = imageMapper.fileToImage(graphic.getFile());
        return image;
    }

    public void deleteImage (){
        dataBase.getGraphicList().remove(index);
    }

    public boolean downloadImage (){
        File file = dataBase.getGraphicList().get(index).getFile();
        try {
            byte[] fileToWrite = Files.readAllBytes(file.toPath());
            outputStream = new FileOutputStream("C:\\Download\\" + file.getName());
            outputStream.write(fileToWrite);
            outputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
