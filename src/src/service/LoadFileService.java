package src.service;

import src.db.DataBase;
import src.model.Graphic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Stateless
public class LoadFileService {

    @EJB
    private DataBase dataBase;

    private BufferedImage image;

    public boolean loadFile(File file) {
        if (validate(file)) {
            dataBase.getGraphicList().add(new Graphic(file));
            return true;
        } else {
            return false;
        }
    }

    private boolean validate(File file) {
        if (file.getName().endsWith(".jpg") && file != null) {
            try {
                image = ImageIO.read(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (image != null)
                return true;
        }
        return false;
    }
}
