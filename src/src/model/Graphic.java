package src.model;

import java.io.File;

public class Graphic {
    private File file;

    public Graphic(File graphicFile) {
        this.file = graphicFile;
    }

    public File getFile() {
        return file;
    }
}
