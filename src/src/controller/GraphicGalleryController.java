package src.controller;

import src.service.GraphicGalleryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/graphicGallery")
public class GraphicGalleryController extends HttpServlet {

    @EJB
    private GraphicGalleryService graphicGalleryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setAttribute("imageList", graphicGalleryService.getImageList());
        req.getRequestDispatcher("graphicGallery.jsp").forward(req,resp);
    }
}
