package src.controller;

import org.apache.commons.io.FileUtils;
import src.service.LoadFileService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

@WebServlet("/home")
@MultipartConfig
public class HomeController extends HttpServlet {

    @EJB
    private LoadFileService loadFileService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(resp.encodeRedirectURL("home.jsp"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("file");
        InputStream fileContent = filePart.getInputStream();
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        File file = new File(fileName);

        try {
            FileUtils.copyInputStreamToFile(fileContent, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (loadFileService.loadFile(file)) {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp?invalidData"));
        }
    }
}
