package src.controller;

import src.db.DataBase;
import src.service.SingleGraphicFileService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/singleGraphicImage")
public class SingleGraphicFileController extends HttpServlet {

    @EJB
    private SingleGraphicFileService singleGraphicFileService;

    @EJB
    private DataBase dataBase;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!getParameter(req,resp))
            resp.sendRedirect(resp.encodeRedirectURL("graphicGallery?notFound"));
    }

    private boolean getParameter (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (int i = 0; i < dataBase.getGraphicList().size(); i++) {
            if (req.getParameter("" + i) != null) {
                req.setAttribute("image", singleGraphicFileService.singleImage(i));
                req.getRequestDispatcher("singleGraphicImage.jsp").forward(req, resp);
                return true;
            } else if (req.getParameter("delete") != null) {
                singleGraphicFileService.deleteImage();
                resp.sendRedirect(resp.encodeRedirectURL("graphicGallery"));
                return true;
            } else if (req.getParameter("download") != null) {
                if (singleGraphicFileService.downloadImage()){
                    resp.sendRedirect(resp.encodeRedirectURL("graphicGallery?downloadOk"));
                    return true;
                }
            }
        }
        return false;
    }
}
