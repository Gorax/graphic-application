<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Graphic gallery</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand active" href="">Graphic application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="graphicGallery">Graphic gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <h1 class="my-4 text-center text-lg-left">Gallery</h1>
    <%
        String downloadOk = request.getParameter("downloadOk");
        String notFound = request.getParameter("notFound");
        if (downloadOk != null) {
    %>
    <small class="form-text text-muted">Your graphics was successes download to C:Download</small>
    <%
        }else if (notFound != null){
    %>
    <small class="form-text text-muted">Something went wrong while downloading.</small>
    <%
        }
    %>
    <br>

    <div class="row text-center text-lg-left">

        <%
            List<String> imageList = (List<String>) request.getAttribute("imageList");
            for (int i = 0; i < imageList.size(); i++) {
        %>
        <div class="col-lg-3 col-md-4 col-xs-6">
            <a href="singleGraphicImage?<%=i%>" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="<%=imageList.get(i)%>" alt="something went wrong ;D">
            </a>
        </div>
        <%
            }
        %>
    </div>

</div>

</body>

</html>
